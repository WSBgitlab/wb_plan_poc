### WB Med

<p>Projeto para apresentação (POC)</p>



### Ferramentas utilizadas na aplicação:

- [Reactjs](https://pt-br.reactjs.org) - Framework Front-end
- [Reactjs-Bootstrap](https://react-bootstrap.github.io) - Framework Front-end css

### Ferramentas extras que ajudaram no desenvolvimento:
- [Insomnia](https://insomnia.rest/) - Utilizado para debugar e testar as rotas da API.dados Postgres.
---

## Como instalar?

Antes de tudo, instale o [Yarn](https://classic.yarnpkg.com/pt-BR/docs/install/) e o [Node.js](https://nodejs.org/en/download/).


1. Git clone:
```
  git clone https://gitlab.com/WSBgitlab/wb_plan_poc.git
```

2. Com o projeto descompactado e dentro do diretório (para instalação dos pacotes):
```
  yarn ou npm install
```

3. Dentro da pasta do projeto executar :
```
  yarn start ou npm start
```

4. Finalizado.
```
  Acesse o link gerado pelo seu terminal.
```

By [Wellington da Silva Bezerra](https://www.linkedin.com/in/wellington-bezerra-dev/)
