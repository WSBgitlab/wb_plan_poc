const express = require("express");

// Modulo para configurações de variáveis de ambiente
const dotenv = require("dotenv");

// Cors server para configurações nas rotas e permissões nas requisições
const cors = require("cors"); 

//  App recebe o express como nosso webserver
const app = express();

//Iniciando variáveis de ambiente configuradas no arquivo .env
dotenv.config();

// Rota para apresentação dos planos enviado do banco de dados.
app.post('/planos/',(req,res,next) => res.send('rota para buscar planos!'));
// Web server escutando na porta 3333.
app.listen(3333);
