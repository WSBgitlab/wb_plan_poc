
/**
 *  Construir um array com as cidades e suas capitais.
 * 
 *  Assim não deixamos redundância no sistema para o usuário.
 * 
 *  Quando ele escolher sua cidade buscar a capital.
 */
export const capitais = [
    {
        capital : 'Acre',
        UF : 'AC'
    },
    {
        capital : 'Alagoas',
        UF: 'AL'
    },
    {
        capital : 'Amapá',
        UF: 'AP'
    },
    {
        capital : 'Amazonas',
        UF: 'AM'
    },
    {
        capital : 'Bahia',
        UF: 'BA'
    },
    {
        capital : 'Ceará',
        UF: 'CE'
    },
    {
        capital : 'Distrito Federal',
        UF: 'DF'
    },
    {
        capital : 'Espírito Santo',
        UF: 'ES'
    },
    {
        capital : 'Goiás',
        UF: 'GO'
    },
    {
        capital : 'Maranhão',
        UF: 'MA'
    },
    {
        capital : 'Rio de Janeiro',
        UF : 'RJ'
    },
    {
        capital : 'Rio Grande do Norte',
        UF : 'RN'
    },
    {
        capital : 'Piauí',
        UF : 'PI'
    },
    {
        capital : 'Pernambuco',
        UF : 'PE'
    },
    {
        capital : 'Pará',
        UF : 'PR'
    },
    {
        capital : 'Paraíba',
        UF : 'PB'
    },
    {
        capital : 'Paraná',
        UF : 'PA'
    },
    {
        capital : 'Minas Gerais',
        UF : 'MG'
    },
    {
        capital : 'Mato Grosso do Sul',
        UF : 'MS'
    },
    {
        capital : 'Mato Grosso',
        UF : 'MT'
    },
    {
        capital : 'Tocantins',
        UF:'TO'
    },
    {
        capital : 'Sergipe',
        UF:'SE'
    },
    {
        capital : 'São Paulo',
        UF:'SP'
    },
    {
        capital : 'Santa Catarina',
        UF:'SC'
    },
    {
        capital : 'Boa Vista',
        UF:'RR'
    },
    {
        capital : 'Roraima',
        UF:'RO'
    },
    {
        capital : 'Rio Grande do Sul',
        UF:'RS'
    }
]