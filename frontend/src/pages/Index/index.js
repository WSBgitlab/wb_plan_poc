import React from 'react';

import { Button , Nav, Navbar, NavLink} from 'react-bootstrap';

import Lottie from 'lottie-react-web';

import Animation from './../../assets/pulse_white.json';

import IndexStyled from './styled';

import Menu from './../Menu/index';

export default function Index(){
    localStorage.clear();
    return (
        <div>
            <Menu />
            <IndexStyled>
                <div className="col col-md-12 grid">
                    <div className="col col-md-6 boxes" id="pulse">
                        <div className="box-info" >
                            <Lottie
                                options={{
                                    animationData: Animation
                                }}
                            />

                        </div>
                    </div>
                    <div className="col col-md-6 boxes">
                        <Nav>
                            <div className="info">
                                <h3>Acesse para encontrar o Plano ideal!</h3>
                                <hr></hr>
                                <Navbar>
                                    <NavLink href="/Planos#stepOne">
                                        <Button className="btn-f or-planes">Buscar Planos</Button>
                                    </NavLink>
                                </Navbar>
                            </div>
                        </Nav>
                    </div>
                </div>
            </IndexStyled>
        </div>
    )
}