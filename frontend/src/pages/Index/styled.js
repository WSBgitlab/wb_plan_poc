import Styled from 'styled-components';


const IndexStyled = Styled.div`
    height: 100vh;
    div.grid{
        background-color: #ffff;
        display:flex;
        padding:0;
        height:100%;
        
        div.boxes {
            padding:0;
            div.box-info{
                background-color:transparent;
                align-items:center;
                text-align:center;
                display:flex;
                position: relative;
        
                div.info{
                    position: absolute;
                    top:40%;
                    right:0;
                    left:0;
                    text-align:center;
                    color:#f1f1f1f7;
                }
            }
        }

        div.boxes + div.boxes{
            display:flex;
            align-items:center;
            justify-content: center;
            padding:0;


            div.nav{
                background-color:#C2E0F2;
                padding:25px;
                width:100%;
                border:1px transparent;
                border-radius: 6px;
                h3{
                    color:#145a5dfa;
                    font-size:20px;
                }
                hr{
                    height:5px;
                    border:1px solid #9CBCD9;
                    background-color:#9CBCD9;
                    border-radius: 6px;
                }
                button {
                    background-color:#9CBCD9;
                    border:1px solid #9CBCD9;
                }
            }

        }
    }

    @media only screen  and (max-width:500px){
        #pulse{
            display:none;
        }

        div.boxes + div.boxes{
            padding:0;
            
            div.nav{
                padding:1em !important;
                margin-top:50px;
            }
        }

    }

    @media only screen and (max-width:450px){
        #pulse{
            display:none;
        }
        display:flex;
        div.boxes + div.boxes{
            height: 20%;
            div.nav{
                padding:0;
                margin-top:0;
            }

            height: 100%;
            margin: 1em;
        }

    }

`;


export default IndexStyled;