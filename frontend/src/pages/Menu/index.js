import React from 'react';

import { Navbar, Nav, Button } from 'react-bootstrap';

import Logo from './../../images/logo_doctor.png';

import MenuStyled from './styled';

import HamburgerMenu from './../../images/hamburger.png';

export default function Menu(){
    return(
        /**
         *  Esse componente é a página inicial do sistema
         *  
         */
        <>
            <MenuStyled>
                <Navbar className="menu" variant="dark">
                    <img src={HamburgerMenu} alt="Menu"></img>
                    <Navbar.Brand className="first-link-logo" href="#home">
                    </Navbar.Brand>
                    <Nav className="links mr-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                        <Nav.Link href="/Planos#stepOne">Planos</Nav.Link>
                        <Nav.Link href="#">Contato</Nav.Link>
                    </Nav>
                </Navbar>
            </MenuStyled>
        </>
    );
}