import Styled from 'styled-components';


const menuStyled = Styled.div`
    
    nav.menu{
        background-color:#68828C;
        display: flex;
        justify-content: center;
        
        img{
            width:30px;
        }

        div.links{
            margin: 0 auto;

        }
        
        > a{
            position:relative;
            > img{
                margin-left:1.5em;
            }

            
        }
        
            
    }
`;


export default menuStyled;