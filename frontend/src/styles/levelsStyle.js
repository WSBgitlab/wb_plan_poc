import Styled from 'styled-components';


const Div = Styled.div`
    div.level-boxes{
        display: inline-block;
        width: 90%;
        margin: 0 auto;
        
        form{
	        display: flex;
            width: 70%;
            height:100%;
            flex-direction: column;
            margin: 0 auto;
            text-align: left;
            
            label.label-menu{
                color:#FFF;
                font-size: 1.5em;

                hr{
                    background:#68828C;
                    height:5px;
                    border:3px transparent;
                    border-radius: 5px;
                }
            }
            label{
                color:#ccc;
                font-size: 1.5em;

                hr{
                    background:#68828C;
                    height:5px;
                    border:3px transparent;
                    border-radius: 5px;
                }
            }
            label.label-menu{
                color:#FFF;
                font-size: 1.5em;

                hr{
                    background:#68828C;
                    height:5px;
                    border:3px transparent;
                    border-radius: 5px;
                }
            }

            label.label-data{
                color:#FFF;
                font-size: 1.5em;

                hr{
                    background:#68828C;
                    height:5px;
                    border:3px transparent;
                    border-radius: 5px;
                }
            }
            
            input, select{
                background:transparent;
                border:3px solid #68828C;
                color:#fff;
                text-transform: capitalize;
                option{
                    background: #374d55;
                }
                &:focus{
                    box-shadow:0 0 0 0.2rem rgba(0, 0, 0, 0.25);
                }
            }

            button{
                margin: 1rem auto;
                display:block;
            }
        }
        form.active-form{
            display:flex;
            h3{
                color:#C2E0F2
            }
        }
        form.disable-form{
            display:none;
        }


        div.entidade{
            width:100%;
            height:100%;
            display: flex;
            flex-direction: column;
            justify-content: space-evenly;
            
            h3{
                color:#C2E0F2
            }
            div.entidade-row{

                ul{
                    list-style:none;
                    padding:0;

                    li{
                        margin:1em;
                        div.card{
                            background-color:#F2F2F2;

                            h5{
                                color:#262425;
                            }

                            button{
                                margin: 0;
                            }
                        }
                    }
                }
            }
        }
    }

    @media only screen and (min-width:300px){
        div.level-boxes form{
            width:100%;
        }

        div.level-boxes form input,div.level-boxes form select{
            width:80%;
        }
    }

`

export default Div;