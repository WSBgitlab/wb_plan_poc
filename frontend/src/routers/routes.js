import React from 'react';

import { BrowserRouter , Route , Switch } from 'react-router-dom';

import Index from './../pages/Index/index';

import Planos from './../components/Planos/index';

import ExibirPlanos from './../components/ExibirPlanos/index';


export default function Routers(){
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Index}></Route>
                <Route path="/Planos" component={Planos}></Route>
                <Route path="/procurarPlanos/" component={ExibirPlanos}></Route>
            </Switch>
        </BrowserRouter>
    );
}