import React from 'react';

//Components
import Menu from './../../pages/Menu/index';

import Steps from '../Steps/index';

export default function Planos(){
    return (
        <>
            <Menu />
            <Steps />
        </>
    )
}