import React, { useEffect , useState} from 'react';

import Planos from './../Planos-paginate/index';

import Pagination from './../Pagination/index';

import { useHistory } from 'react-router-dom';

import { ApiQualicorpPost } from './../../config/https';

import Menu from './../../pages/Menu/index';

import Styled from './styled';

export default function ExibirPlanos( props ) {
  const [planos, setPlanos] = useState([]);
  const [loading, setLoad] = useState(false);
  
  const [currentPage, setCurrentPage] = useState(1);
  const [PlanosPerPages] = useState(5);

  const history = useHistory();

  useEffect(() => {
    const fetchProps = async () => {
      setLoad(true);
      const response = await ApiQualicorpPost.post('plano?api-key=47acfdec-048b-40a1-b826-d867199ac786',{
        entidade: localStorage.getItem('NomeFantasia'),

        uf: localStorage.getItem('uf'),

        cidade: localStorage.getItem('estado'),

        datanascimento: [localStorage.getItem('dtNascimento')]
      });
      
      setPlanos(response.data.planos);

      setLoad(false);
    }
    fetchProps();
  },[])

  // Obtendo os Planos atuais

  /**
   * 
   * Irá realizar a multiplicação entre a página atual e a página total no qual você decidiu mostrar os dado
   * nesse casos 10.
   */

  const indexOfLastPost = currentPage * PlanosPerPages;

  const indexOfFirstPost = indexOfLastPost - PlanosPerPages;
  
  /**
   * Aqui estamos juntando os dois dados dentro de um array.
   * 
   * currentPlanos irá receber indexOfLastPost o número de início que é no primeiro momento:
   * 
   * currentPage = 1 - PlanosPerPages = 10
   * 
   * 1 x 10 = 10 
   * 2 x 10 = 20
   * 
   * Logo após recebe para voltar a posição de início.
   * 
   * 10 - 10 (1 x 10 = 10) = 0 
   * 20 - 10 (2 x 10 = 20) = 10
   * 
   */
  const currentPlanos = planos.slice(indexOfFirstPost, indexOfLastPost);

  /**
   * Mudando de página
   */

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
      <Styled>
        <Menu />
          <div className="container mt-4 col col-md-12 exibir-planos">
              <Planos 
                planos={currentPlanos} 
                loading={loading}
              />

              <Pagination 
                planosPerPage={PlanosPerPages} 
                totalPlanos={planos.length} 
                paginate={paginate}
              />
          </div>
      </Styled>
  );
}