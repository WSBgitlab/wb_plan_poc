import Styled from 'styled-components';

const div = Styled.div`
    height:100%;

    div.exibir-planos{
        padding:0;
        margin:0 !important;
        background:#0e252e;
    }

`;

export default div;