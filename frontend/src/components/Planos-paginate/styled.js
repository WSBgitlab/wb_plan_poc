import Styled from 'styled-components';


const div = Styled.div`
        width: 50%;
        margin: 0 auto;
        padding: 15px 0;
        ul.card-planos{
            display:flex;
            padding: 0;
            li{
                div.card{

                }

                div.card-body{
                    blockquote{
                        text-align:center;
                        img{
                            width:50%;
                        }
                    }

                    ul{
                        list-style:none;
                        li{
                            i{
                            }
                        }

                        &:last-child li{
                            display:flex;
                            justify-content:space-between;
                        }
                    }
                }
            }
        }
    @media only screen and (max-width:500px){
        background-color:#09242ecf;
        width: 100%;
        ul.card-planos{
            display:flex;
            padding: 0;
            li{
                margin:1em;
                padding: 0;
                div.card{

                }

                div.card-body{
                    padding:0;
                    blockquote{
                        text-align:center;
                        img{
                            width:50%;
                        }
                    }

                    ul{
                        list-style:none;

                        li{
                            margin:1em;

                            i{
                                margin: 0 25px;
                                text-align: justify;
                            }
                        }

                        &:last-child li{
                            display:flex;
                            justify-content:space-between;
                        }
                    }
                }
            }
        }


        div.exibir-planos{
            display: flex;
            flex-direction: column;
            width: 80%;
            margin:0px;
            padding:0;
        }
    }
`;


export default div;
