import React from 'react'

import { Card, Button, Spinner} from 'react-bootstrap';

import Styled from './styled';
 
const Posts = ({ planos , loading}) => {
    if(loading) return <Spinner animation="grow" variant="info" />
    
    return (
        <Styled>
            <ul className="list-group mb-4 card-planos">
                {
                    planos.map(dados => (
                        <li 
                            key={dados.id}
                            className="list-group-item"
                        >
                            <Card>
                                <Card.Header>
                                    {dados.plano}
                                </Card.Header>
                                    <Card.Body>
                                        <blockquote className="blockquote mb-0">
                                            <img src={dados.operadoraLogo}></img>
                                        </blockquote>
                                        <ul>
                                            <li> 
                                                <b>Abrangencia</b> {dados.abrangencia}
                                            </li>
                                            <li>
                                                <b>Acomodação</b> {dados.tipo_acomodacao}
                                            </li>
                                            <li>
                                                <b>Segmentação</b> 
                                                <hr></hr>
                                                <i>
                                                    {dados.segmentacao}
                                                </i>
                                            </li>
                                            <br></br>
                                            <li>
                                                <i><b>Valor</b></i>

                                                <Button variant="outline-success">{dados.precos.total}</Button>
                                            </li>
                                        </ul>
                                    </Card.Body>
                            </Card>
                        
                        </li>
                    ))
                }
            </ul>
        </Styled>
    )
}

export default Posts;