import React from 'react';


import Styled from './styled';

const Pagination = ({ planosPerPage , totalPlanos, paginate}) => {
    const pageNumber = [];

    for(let i = 1; i <= Math.ceil(totalPlanos / planosPerPage);++i){
        pageNumber.push(i);
    }
    
    return (
        <Styled>
            <nav>
                <ul className="pagination">
                    {
                        pageNumber.map(number => (
                            <li key={number} className="page-item">
                                <a onClick={() => paginate(number)} href="!#" className="page-link">
                                    {number}
                                </a>
                            </li>
                        ))
                    }
                </ul>
            </nav>
        </Styled>
    )

}

export default Pagination;