import Styled from 'styled-components';


const div = Styled.div`
    nav{
        padding:4rem;
        ul.pagination{
            display: flex;
            padding: 0;
            margin: 0 auto;
            justify-content: center;
            overflow-x: auto;
        }
    }
`;

export default div;


