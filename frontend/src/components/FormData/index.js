import React, { useState , useEffect } from 'react';

import { Form, Button, Spinner, Card } from 'react-bootstrap';

import LevelStyles from '../../styles/levelsStyle';

import { useHistory } from "react-router-dom";

import { ApiQualicorp , ApiWbmed } from '../../config/https';

import { Link } from 'react-router-dom';

import InfiniteCalendar from 'react-infinite-calendar';

import { capitais } from './../../utils/capitais';

import { cardError } from './../../config/cards';

import './../../styles/styles-infinite-calendar.css'

export default function FaseOne( props ){
    /**
     * Fase 1 - Rquerindo informações do usuário.
     */
    const [nome, setNome]= useState('')
    const [estado, setEstado]= useState('')
    const [date, setDate] = useState([]);

    //Loaging
    const [loading, setLoading] = useState(false);
    /**
     * Dados retornados pela api Qualicorp
     */

     // Apresentações para usuário escolher sua profissão.
    const [profissoes,setProfissoes] = useState([]);

    // Variável irá armazenar entidades retornadas.
    const [entidade,setEntidade] = useState([])

    // Variável para armazenar a escolha de profissão do usuário.
    const [profissaoSelecionada, setProfissaoSelecionada] = useState('')

    // Fases do sistema.
    const [stepTwo, setStepTwo] = useState(false)
    const [stepThree, setSThree] = useState(false)

    // historico de atributos do component e ciclo de vida;
    const history = useHistory();

    /**
     * Handle Component quando a segunda fase é iniciada.
     */


    useEffect(() => {
        async function getProfissoes(){
            setLoading(true)
            const response = await ApiQualicorp.get(`profissao/${localStorage.getItem('uf').toString()}/${localStorage.getItem('estado').toString()}?api-key=${process.env.REACT_APP_API_TOKEN}`)
            
            setProfissoes(response.data)
            setLoading(false)
        }

        return () => getProfissoes()
    },[stepTwo, history.location])

    /**
     * Handle Component quando a terceira fase é iniciada.
     */

     useEffect(() => {
        async function getEntidaes(){
            setLoading(true);
            
            const response = await ApiQualicorp.get(`entidade/${localStorage.getItem('profissao')}/${localStorage.getItem('uf')}/${localStorage.getItem('estado')}?api-key=${process.env.REACT_APP_API_TOKEN_ENTIDADES}`)
            
            
            setEntidade(response.data);
            setLoading(false);
        }

        getEntidaes();
     },[stepThree])


    /**
     *  Fase 1
     *  
     *  Essa função será responsável por amarzenar entradas do usuário, no database do browser
     *  e iniciar a fase 2.
     */

    const capitaisFilter = capitais.filter(item => item.capital === estado);

    // localStorage.setItem('uf',capitaisFilter.UF);
    console.log(capitaisFilter)

    const handleStepsOne = (event) => {
        event.preventDefault();
        // Validação de campos
        // campo nome 


        if(nome === ""){
            cardError("Preencha o nome!");
        } else if (date.length === 0){
            cardError("Preencha a data!");
        }else if(estado === ""){
            cardError("Preencha a estado!");
        }else{
            
            // Next Fase
            setStepTwo(true)

            // Procurando capitais em arquivo de controle.
            const capitaisFilter = capitais.filter(item => item.capital === estado);
    
            // Setando uf no database.
            localStorage.setItem('uf',capitaisFilter[0].UF);
    
            // Mudando url para Fase 2
            history.push('/Planos#stepTwo');
        }
    }

    /**
     * Fase 2
     * 
     * Irá iniciar a busca em api da qualicorp. Assim irá buscar as entidades do estado, capital e profissão 
     * que o usuário cadastrou.
     */

    const handleStepsTwo = event =>{
        event.preventDefault();

        // Atribuindo profissão ao database do browser.
        localStorage.setItem('profissao', profissaoSelecionada);

        // Próxima fase
        setSThree(true);

        // Mudando url para Fase 3
        history.push('/Planos#stepThree');

    }

    return (
        /**
         *  Será o formulário para obter cidade e estado.
         */
        <>
            <LevelStyles>
                <div className="level-boxes">
                    {/* Primeira fase pedir capital e UF  */}
                    <Form  
                        className={history.location.hash === '#stepOne' ? 'active-form' : 'disable-form'} 
                        onSubmit={handleStepsOne}>
                    <Form.Group>
                        <Form.Label className="label-menu">Nome
                            <hr></hr>
                        </Form.Label>
                        <Form.Control
                            type="text" 
                            placeholder="Seu Nome"
                            onChange={ event => {
                                setNome(event.target.value)
                            }}
                            autoComplete="off"
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label className="label-data">Data de Nascimento
                            <hr></hr>
                        </Form.Label>
                        <InfiniteCalendar
                            onSelect={(selectDates) => {
                                // Pegando o retorno string e convertendo para data.
                                const dataNova = new Date(selectDates);

                                let dia = dataNova.getDate()
                                let mes = dataNova.getMonth() + 1;
                                let ano = dataNova.getFullYear()
                                
                                if(mes <= 9){
                                    mes = `0${mes}`;
                                }

                                if(dia <= 9){
                                    dia = `0${dia}`;
                                }

                                // setando data no formato indicado.
                                localStorage.setItem('dtNascimento',[`${ano}-${mes}-${dia}`])
                                console.log(date)
                                setDate([`${ano}-${mes}-${dia}`])
                            }}
                            width="100%"
                            height={200}
                            onChange={ event => setDate(event.target.value)}
                            
                        />,
                        <br></br>
                    </Form.Group>

                    <Form.Group 
                        controlId="Capitais"
                        onChange={ event => localStorage.setItem('estado',event.target.value)}
                    >
                        <Form.Label className="label-menu">
                            Sua Capital.
                            <hr></hr>
                        </Form.Label>
                        <Form.Control 
                            as="select"
                            onChange={ event => setEstado(event.target.value)}
                        >
                            <option value="Acre">Acre</option>
                            <option value="Alagoas">Alagoas</option>
                            <option value="Amapá">Amapá</option>
                            <option value="Amazonas">Amazonas</option>
                            <option value="Bahia">Bahia</option>
                            <option value="Ceará">Ceará</option>
                            <option value="Distrito Federal">Distrito Federal</option>
                            <option value="Espírito Santo">Espírito Santo</option>
                            <option value="Goiás">Goiás</option>
                            <option value="Maranhão">Maranhão</option>
                            <option value="Mato Grosso">Mato Grosso</option>
                            <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                            <option value="Minas Gerais">Minas Gerais</option>
                            <option value="Pará">Pará</option>
                            <option value="Paraíba">Paraíba</option>
                            <option value="Paraná">Paraná</option>
                            <option value="Pernambuco">Pernambuco</option>
                            <option value="Piauí">Piauí</option>
                            <option value="Rio de Janeiro">Rio de Janeiro</option>
                            <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                            <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                            <option value="Rondônia">Rondônia</option>
                            <option value="Roraima">Roraima</option>
                            <option value="Santa Catarina">Santa Catarina</option>
                            <option value="São Paulo">São Paulo</option>
                            <option value="Sergipe">Sergipe</option>
                            <option value="Tocantins">Tocantins</option>
                        </Form.Control>

                    </Form.Group>
                        <Button type="submit" variant="light">Buscar</Button>
                    </Form>

                    {
                        // Animação de loading;
                        loading === true && stepTwo === true ? <Spinner animation="grow" variant="info" />:''
                    } 
            
                        
                    { /* Segunda fase */}
                    <Form 
                        className={history.location.hash === '#stepTwo' ? 'active-form' : 'disable-form'}
                        onSubmit={handleStepsTwo}
                    >
                        <Form.Group>
                            <Form.Label>Escolha sua Profissão!
                                <hr></hr>
                            </Form.Label>
                            <Form.Control 
                                as="select" 
                                onChange={(event) => setProfissaoSelecionada(event.target.value)}
                            >
                                { profissoes.map(items => <option key={items.profissao} value={items.profissao}>{items.profissao}</option>)}
                            </Form.Control>

                            <Button type="submit" variant="light">Buscar</Button>
                        </Form.Group>
                    </Form>

                    
                    { /* Terceira fase */}
                    <Form 
                        className={history.location.hash === '#stepThree' ? 'active-form' : 'disable-form'}
                    >
                        <Form.Group className="entidade">
                            <Form.Label>
                                Escolha sua Entidade de Plano de Saúde.
                                <hr></hr>
                            </Form.Label>
                                <div className="row entidade-row">
                                    {
                                        loading === true ? <Spinner animation="grow" variant="info" />:''
                                    }
                                    <ul>
                                        { entidade.map(data => (
                                            <li>
                                                <Card>
                                                    <Card.Header as="h5">{data.NomeFantasia}</Card.Header>
                                                        <Card.Body>
                                                            <Card.Title>Razão Social</Card.Title>
                                                            <Card.Text>
                                                               {data.RazaoSocial}
                                                            </Card.Text>
                                                            <Link 
                                                                style={{ textDecoration : "none", margin :'0.5em'}} 
                                                                to={{
                                                                    pathname : `/procurarPlanos/`,
                                                                    state : {
                                                                        ...data,
                                                                        nome : nome,
                                                                        dtNascimento : date
                                                                    }
                                                                }}
                                                            >
                                                            <Button variant="dark"value={data.NomeFantasia} onClick={event => localStorage.setItem('NomeFantasia',event.target.value)}>Escolher</Button>
                                                        </Link>
                                                    </Card.Body>
                                                </Card>   
                                            </li>
                                            )
                                        )}
                                    </ul>
                                </div>
                        </Form.Group>
                    </Form>
                </div>
            </LevelStyles>
        </>
    )
}