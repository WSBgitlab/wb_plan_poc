import React, { useEffect, useState } from 'react';

import { Nav } from 'react-bootstrap';

import { useLocation } from 'react-router-dom';

import Styled from './styled';

import FormOne from './../FormData/index';

export default function Steps(){
    // Qual rota o usuário está
    const location = useLocation();
    
    useEffect(() =>{
        // Handle component when url update.
    },[location.hash])

    return(
        <>
            <Styled>
                <div className="container box-content header">
                    <h2>Vamos encontrar o plano ideal?</h2>
                    <Nav>
                        <Nav.Item>
                            <Nav.Link href="#stepOne" className={ location.hash === '#stepOne' ? 'active':'disable'}>
                                <b>Fase 1</b>
                                <hr className={ location.hash === '#stepOne' ? 'active' : 'disable'}></hr>
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link href="#stepTwo" className={ location.hash === '#stepTwo' ? 'active':'disable'}>
                                <b>Fase 2</b>
                                <hr className={ location.hash === '#stepTwo' ? 'active' : 'disable'}></hr>
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link href="#stepThree" className={ location.hash === '#stepThree' ? 'active' : 'disable'}>
                                <b>Fase 3</b>
                                <hr className={ location.hash === '#stepThree' ? 'active' : 'disable'}></hr>
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>
                </div>
                <div className="container box-content">
                    <FormOne></FormOne>
                </div>
            </Styled>       
        </>
    );
}