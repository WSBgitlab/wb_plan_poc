import Styled from 'styled-components';

const formStyled = Styled.div`
    width:100%;
    background:#F2F2F2;
    text-align:center;
    padding:4em 0 4em 0;
    
    hr.active{
        background: #f36c42
    }

    a.disable{
        pointer-events: none;
        b{
            color: #748991;
        }
    }

    div.box-content{
        padding:2em 0;
        max-width: 70%;
        background:#09242ecf;
        border:1px transparent;

        div{
            form.active-form{
                height:100%;
            }
        }
    }
    
   
    div.header{
        h2{
            color:#C2E0F2;
        }

        div.nav{
            text-align:center;
            display:flex;
            justify-content:center;

            div.nav-item{
                a{
                    color:#fff;
                    transition: 2s all;
                    border-bottom: 5px transparent;

                    
                }
                hr.active{
                    height: 5px;
                    border: 1px solid transparent;
                    border-radius: 25px;
                    width: 100%;
                    transition: 0.5s all;
                    background-color: #f36c42;
                }
            }
        }
    }
    

    hr.active{
        height: 5px;
        border: 1px solid transparent;
        border-radius: 25px;
        width: 100%;
        transition: 2s all;
        background-color: #f36c42;
    }

    div.div.Cal__Container__root{
        width:
    }

    @media only screen and (max-width:500px){
        div.box-content{
            max-width:100% ;
        }

        div.header h2, div.level-boxes form label{
            font-size:20px !important;
        }
    
        padding:0;

    }
`;

export default formStyled;