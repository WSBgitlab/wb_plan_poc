import http from "axios";

// Headers GET
const headersGet = {
    'Content-Type':'application/json',
    'Content': 'application/json',
    'api-token' : 'eebc059d-6838-4762-8e28-d2f726753866'
}

// Headers POST
const headersPost = {
    'Content-Type':'application/json',
    'Content': 'application/json',
    'api-token' : '47acfdec-048b-40a1-b826-d867199ac786'
}

// GET
export const ApiQualicorp = http.create({
    baseURL : 'https://apisimulador.qualicorp.com.br/',
    headersGet
})

export const ApiWbmed = http.create({
    baseURL : 'http://localhost:5555'
});


export const ApiQualicorpPost = http.create({
    baseURL : 'https://apisimulador.qualicorp.com.br/',
    headersPost
})
