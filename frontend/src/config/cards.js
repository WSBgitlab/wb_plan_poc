import { toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

export async function cardSucess(msg) {
    return toast.success(`${msg}! 😀👍🏼`);
}

export async function cardError(msg){
    return toast.error(`Error : ${msg} 😬`);
}
